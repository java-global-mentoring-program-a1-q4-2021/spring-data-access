create table events
(
    id identity,
    date         timestamp,
    title        varchar,
    ticket_price decimal
);

create table user_accounts
(
    id identity,
    amount decimal
);

create table users
(
    id identity,
    name      varchar,
    email     varchar,
    amount_id bigint,
    foreign key (amount_id) references user_accounts (id)
);

create table tickets
(
    id identity,
    event_id bigint,
    user_id  bigint,
    place    integer,
    category varchar,
    foreign key (event_id) references events (id),
    foreign key (user_id) references users (id)
);