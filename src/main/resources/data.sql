insert into user_accounts(amount)
values (11111),
       (22222),
       (33333),
       (44444);

insert into users(name, email, amount_id)
values ('Bob', 'bob@mail.com', 1),
       ('Sasha', 'sasha@mail.com', 2),
       ('Sveta', 'sveta@mail.com', 3),
       ('Maksim', 'maksim@mail.com', 4);

insert into events(date, title, ticket_price)
values ('2022-01-13 15:00:00', 'Eugene Onegin performance', 120),
       ('2022-01-13 10:00:00', 'The Idiot', 140);

insert into tickets(event_id, user_id, place, category)
values (1, 1, 11, 'PREMIUM'),
       (2, 2, 22, 'PREMIUM');