package com.hellospringmvc.facade;

import com.hellospringmvc.model.Event;
import com.hellospringmvc.model.Ticket;
import com.hellospringmvc.model.TicketCategory;
import com.hellospringmvc.model.User;
import com.hellospringmvc.service.EventService;
import com.hellospringmvc.service.TicketService;
import com.hellospringmvc.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service("bookingFacade")
public class BookingFacadeImpl implements BookingFacade {

    private final EventService eventService;
    private final TicketService ticketService;
    private final UserService userService;

    @Autowired
    public BookingFacadeImpl(EventService eventService,
                             TicketService ticketService,
                             UserService userService) {
        this.eventService = eventService;
        this.ticketService = ticketService;
        this.userService = userService;
    }

    @Override
    public List<Event> getEvents(int pageSize, int pageNum) {
        return eventService.getAll(pageSize,pageNum);
    }
    @Override
    public List<User> getUsers(int pageSize, int pageNum) {
        return userService.getUsers(pageSize,pageNum);
    }
    @Override
    public List<Ticket> getBookedTickets(int pageSize, int pageNum) {
        return ticketService.getTickets(pageSize,pageNum);
    }

    @Override
    public Event getEventById(long eventId) {
        return eventService.getById(eventId).orElse(null);
    }

    @Override
    public List<Event> getEventsByTitle(String title, int pageSize, int pageNum) {
        return null;
    }

    @Override
    public List<Event> getEventsForDay(Date day, int pageSize, int pageNum) {
        return null;
    }

    @Override
    public Event createEvent(Event event) {
        return null;
    }

    @Override
    public Event updateEvent(Event event) {
        return null;
    }

    @Override
    public boolean deleteEvent(long eventId) {
        return false;
    }

    @Override
    public User getUserById(long userId) {
        return userService.getUserById(userId).orElse(null);
    }

    @Override
    public User getUserByEmail(String email) {
        return null;
    }

    @Override
    public List<User> getUsersByName(String name, int pageSize, int pageNum) {
        return null;
    }

    @Override
    public User createUser(User user) {
        return null;
    }

    @Override
    public User updateUser(User user) {
        return null;
    }

    @Override
    public boolean deleteUser(long userId) {
        return false;
    }

    @Override
    public Ticket bookTicket(long userId, long eventId, int place, TicketCategory category) {
        return null;
    }

    @Override
    public Ticket getTicketById(long id) {
        return ticketService.getTicket(id).orElse(null);
    }

    @Override
    public List<Ticket> getBookedTickets(User user, int pageSize, int pageNum) {
        return null;
    }

    @Override
    public List<Ticket> getBookedTickets(Event event, int pageSize, int pageNum) {
        return null;
    }

    @Override
    public boolean cancelTicket(long ticketId) {
        return false;
    }
}
