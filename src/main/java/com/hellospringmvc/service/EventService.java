package com.hellospringmvc.service;

import com.hellospringmvc.model.Event;

import java.util.List;
import java.util.Optional;

public interface EventService {
    Optional<Event> getById(long id);

    List<Event> getByTitle(String title, int pageSize, int pageNum);

    List<Event> getAll(int pageSize, int pageNum);

    Event save(Event event);

    void delete(long eventId);

    void delete(Event event);
}
