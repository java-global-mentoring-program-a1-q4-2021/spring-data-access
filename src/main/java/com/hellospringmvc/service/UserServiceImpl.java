package com.hellospringmvc.service;

import com.hellospringmvc.model.User;
import com.hellospringmvc.model.UserAccount;
import com.hellospringmvc.repository.UserAccountRepository;
import com.hellospringmvc.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;
    private final UserAccountRepository userAccountRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, UserAccountRepository userAccountRepository) {
        this.userRepository = userRepository;
        this.userAccountRepository = userAccountRepository;
    }

    @Transactional
    public void refillingAccount(long userId, BigDecimal amount) {
        getUserById(userId).ifPresent(user -> {
            UserAccount account = user.getAccount();
            account.setAmount(amount.add(account.getAmount()));
            userAccountRepository.save(account);
        });
    }

    @Transactional
    public void withdrawFromAccount(long userId, BigDecimal amount) {
        getUserById(userId).ifPresent(user -> {
            UserAccount account = user.getAccount();
            if (account.getAmount() == null) {
                return;
            }
            account.setAmount(account.getAmount().add(amount));
            userAccountRepository.save(account);
        });
    }

    @Override
    public Optional<User> getUserById(long userId) {
        return userRepository.findById(userId);
    }

    @Override
    public Optional<User> getUserByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    @Override
    public List<User> getUsersByName(String name, int pageSize, int pageNum) {
        return userRepository.findAllByNameContains(name, PageRequest.of(pageNum, pageSize)).getContent();
    }

    @Override
    public List<User> getUsers(int pageSize, int pageNum) {
        return userRepository.findAll(PageRequest.of(pageNum, pageSize)).getContent();
    }

    @Override
    public User saveUser(User user) {
        return userRepository.save(user);
    }

    @Override
    public void deleteUser(long userId) {
        userRepository.deleteById(userId);
    }

    @Override
    public void deleteUser(User user) {
        userRepository.delete(user);
    }
}
