package com.hellospringmvc.service;

import com.hellospringmvc.model.Event;
import com.hellospringmvc.repository.EventRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class EventServiceImpl implements EventService {

    private final EventRepository eventRepository;

    @Autowired
    public EventServiceImpl(EventRepository eventRepository) {
        this.eventRepository = eventRepository;
    }

    @Override
    public Optional<Event> getById(long eventId) {
        return eventRepository.findById(eventId);
    }

    @Override
    public List<Event> getByTitle(String title, int pageSize, int pageNum) {
        return eventRepository.findAllByTitleContains(title, PageRequest.of(pageNum, pageSize)).getContent();
    }

    @Override
    public List<Event> getAll(int pageSize, int pageNum) {
        return eventRepository.findAll(PageRequest.of(pageNum, pageSize)).getContent();
    }

    @Override
    public Event save(Event event) {
        return eventRepository.save(event);
    }

    @Override
    public void delete(long eventId) {
        eventRepository.deleteById(eventId);
    }

    @Override
    public void delete(Event event) {
        eventRepository.delete(event);
    }
}
