package com.hellospringmvc.service;

import com.hellospringmvc.model.User;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

public interface UserService {
    Optional<User> getUserById(long userId);

    Optional<User> getUserByEmail(String email);

    List<User> getUsersByName(String name, int pageSize, int pageNum);

    List<User> getUsers(int pageSize, int pageNum);

    User saveUser(User user);

    void deleteUser(long userId);

    void deleteUser(User user);

    void refillingAccount(long userId, BigDecimal amount);

    void withdrawFromAccount(long userId, BigDecimal amount);
}
