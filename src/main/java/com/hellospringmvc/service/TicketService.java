package com.hellospringmvc.service;

import com.hellospringmvc.model.Ticket;
import com.hellospringmvc.model.TicketCategory;

import java.util.List;
import java.util.Optional;

public interface TicketService {
    Ticket bookTicket(long userId, long eventId, int place, TicketCategory category);

    List<Ticket> getTickets(int pageSize, int pageNum);

    Optional<Ticket> getTicket(long ticketId);
}
