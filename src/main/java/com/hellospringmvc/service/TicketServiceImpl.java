package com.hellospringmvc.service;

import com.hellospringmvc.model.Event;
import com.hellospringmvc.model.Ticket;
import com.hellospringmvc.model.TicketCategory;
import com.hellospringmvc.model.User;
import com.hellospringmvc.repository.TicketRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class TicketServiceImpl implements TicketService {
    private final TicketRepository ticketRepository;
    private final UserService userService;
    private final EventService eventService;

    @Autowired
    public TicketServiceImpl(TicketRepository ticketRepository, UserService userService, EventService eventService) {
        this.ticketRepository = ticketRepository;
        this.userService = userService;
        this.eventService = eventService;
    }

    @Transactional
    @Override
    public Ticket bookTicket(long userId, long eventId, int place, TicketCategory category) {
        Optional<User> userOptional = userService.getUserById(userId);
        Optional<Event> eventOptional = eventService.getById(eventId);
        if (userOptional.isEmpty() || eventOptional.isEmpty()) {
            throw new IllegalArgumentException("user and/or event not found");
        }
        User user = userOptional.get();
        Event event = eventOptional.get();
        if (user.getAccount().getAmount().compareTo(event.getTicketPrice()) == -1) {
            throw new UnsupportedOperationException("not enough money");
        }
        userService.withdrawFromAccount(userId, event.getTicketPrice());
        Ticket ticket = new Ticket();
        ticket.setPlace(place);
        ticket.setCategory(category);
        ticket.setEvent(event);
        ticket.setUser(user);
        return ticketRepository.save(ticket);
    }

    @Override
    public List<Ticket> getTickets(int pageSize, int pageNum) {
        return ticketRepository.findAll(PageRequest.of(pageNum,pageSize)).getContent();
    }

    @Override
    public Optional<Ticket> getTicket(long ticketId) {
        return ticketRepository.findById(ticketId);
    }
}
