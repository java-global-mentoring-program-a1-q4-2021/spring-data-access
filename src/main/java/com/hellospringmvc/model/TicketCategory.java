package com.hellospringmvc.model;

public enum TicketCategory {
    STANDARD, PREMIUM, BAR
}
