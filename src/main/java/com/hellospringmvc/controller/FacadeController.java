package com.hellospringmvc.controller;

import com.hellospringmvc.facade.BookingFacade;
import com.hellospringmvc.model.Event;
import com.hellospringmvc.model.Ticket;
import com.hellospringmvc.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/")
public class FacadeController {

    private final BookingFacade facade;

    @Autowired
    public FacadeController(BookingFacade facade) {
        this.facade = facade;
    }

    @GetMapping
    public String index(Model model) {
        model.addAttribute("message", "Hello Spring MVC!");
        return "index";
    }

    @GetMapping("/tickets")
    public String getTickets(@RequestParam(name = "pageSize", required = false, defaultValue = "5") Integer pageSize,
                             @RequestParam(name = "pageNum", required = false, defaultValue = "0") Integer pageNum,
                             Model model) {
        model.addAttribute("tickets", facade.getBookedTickets(pageSize, pageNum));
        return "tickets";
    }

    @GetMapping("/tickets/{id}")
    public String getTicketById(@PathVariable Integer id, Model model) {
        buildTicketDetails(id, model);
        return "ticket";
    }

    @GetMapping("/tickets/{id}/pdf")
    public ModelAndView getTicketByIdInPdf(@PathVariable Integer id, Model model) {
        buildTicketDetails(id, model);
        return new ModelAndView("viewPDF", "data", model);
    }

    @GetMapping("/users")
    public String getUsers(@RequestParam(name = "pageSize", required = false, defaultValue = "5") Integer pageSize,
                           @RequestParam(name = "pageNum", required = false, defaultValue = "0") Integer pageNum,
                           Model model) {
        model.addAttribute("users", facade.getUsers(pageSize, pageNum));
        return "users";
    }

    @GetMapping("/events")
    public String getEvents(@RequestParam(name = "pageSize", required = false, defaultValue = "5") Integer pageSize,
                            @RequestParam(name = "pageNum", required = false, defaultValue = "0") Integer pageNum,
                            Model model) {
        model.addAttribute("events", facade.getEvents(pageSize, pageNum));
        return "events";
    }

    private void buildTicketDetails(long ticketId, Model model) {
        Ticket ticket = facade.getTicketById(ticketId);
        User user = facade.getUserById(ticket.getUserId());
        Event event = facade.getEventById(ticket.getEventId());
        model.addAttribute("ticket", ticket);
        model.addAttribute("user", user);
        model.addAttribute("event", event);
    }

}
