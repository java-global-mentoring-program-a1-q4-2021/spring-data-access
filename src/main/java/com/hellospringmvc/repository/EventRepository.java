package com.hellospringmvc.repository;

import com.hellospringmvc.model.Event;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface EventRepository extends PagingAndSortingRepository<Event, Long> {
    Page<Event> findAllByTitleContains(String title, Pageable pageable);
}
